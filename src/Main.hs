module Main where

import Parser
import Lexer
import TypeCheck
import Printer

main = do txt <- getContents
          case parse (txt, 0, 0) of
              Right ast -> do
                putStrLn $ showInstrs 0 ast
                -- putStrLn $ show ast
                -- let types = typeCheckAll [] ast
                -- mapM_ (either putStrLn print) types
              Left _ -> print "parsing failed"
          return ()

