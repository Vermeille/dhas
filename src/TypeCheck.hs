#! /usr/bin/env runhugs +l
--
-- Eval.hs
-- Copyright (C) 2013 vermeille <guillaume.sanchez@epita.fr>
--
-- Distributed under terms of the MIT license.
--

module TypeCheck where

import Ast
import Lexer
import Data.List

data Type = TNbr
          | TStr
          | TList Type
          | TBool
          | TPoly
          deriving (Eq, Show)

type Typed = (Located String, Type)

isLeft  = either (const True) (const False)
isRight = either (const False) (const True)

typeCheck :: [Typed] -> Located Exp -> Either String Type

typeCheck env (L _ (Nbr a)) = Right TNbr
{-
typeCheck env (L loc (Var a)) =
    case find ((== a) . unloc . fst) env of
        Just res -> return $ snd res
        Nothing -> Left $ show loc
            ++ ": cannot infer type of variable " ++ a ++ "\n"
-}

typeCheck env (L loc (Add a b))     = typeCheckArithm env loc "+" a b
typeCheck env (L loc (Sub a b))     = typeCheckArithm env loc "-" a b
typeCheck env (L loc (Mul a b))     = typeCheckArithm env loc "*" a b
typeCheck env (L loc (Div a b))     = typeCheckArithm env loc "/" a b
typeCheck env (L loc (Less a b))    = typeCheckCmp env loc "<" a b
typeCheck env (L loc (Great a b))   = typeCheckCmp env loc ">" a b
typeCheck env (L loc (Leq a b))     = typeCheckCmp env loc "<=" a b
typeCheck env (L loc (Geq a b))     = typeCheckCmp env loc ">=" a b
typeCheck env (L loc (ListLit [a])) =
        case typeCheck env a of
            (Right a) -> Right (TList a)
            err -> err
typeCheck env (L loc (ListLit (a:as))) =
        case (typeCheck env a, typeCheck env (L loc (ListLit as))) of
            (Right a, Right b) -> either (Left . err loc) Right $ mergeTy (TList a) b
            _                  -> Left $ err loc ""
            where
                err :: (Int, Int) -> String -> String
                err loc str = str ++ show loc
                    ++ ": List is not consistently typed\n"
typeCheck env (L loc (Paren a)) = typeCheck env a

typeCheck env (L loc (If a b c)) =
    case (tIf, tElse, tThen) of
        (Right TBool, Right a, Right b)
                | a == b    -> Right a
                | otherwise -> Left $ show loc ++ ": bodies type do not match\n"
        (Right _, _, _)     -> Left $ show loc ++ ": if condition must be a bool exp"
        (a, b, c)           -> Left $ getErr a ++ getErr b ++ getErr c
    where [tIf, tThen, tElse] = map (typeCheck env) [a, b, c]
          getErr (Left err) = err
          getErr _ = ""

typeCheckArithm :: [Typed] -> (Int, Int) -> String -> Located Exp
                                        -> Located Exp -> Either String Type
typeCheckArithm env loc nodeTy a b =
    case (typeCheck env a, typeCheck env b) of
        (Right a, Right b)    -> mergeTy a b
        (Left msg, Right _)   -> Left $ err msg loc nodeTy
        (Right _, Left msg)   -> Left $ err msg loc nodeTy
        (Left msg, Left msg') -> Left $ err (msg ++ msg') loc nodeTy
    where err msg loc nodeTy =
                msg ++ show loc ++ ": cannot infer type of " ++ nodeTy ++ "\n"

typeCheckCmp :: [Typed] -> (Int, Int) -> String -> Located Exp
                                        -> Located Exp -> Either String Type
typeCheckCmp env loc nodeTy a b =
    case (typeCheck env a, typeCheck env b) of
        (Right a, Right b) -> mergeCmpTy a b
        (Left msg, Right _)   -> Left $ msg ++ show loc
                    ++ ": cannot infer type of " ++ nodeTy ++ "\n"
        (Right _, Left msg)   -> Left $ msg ++ show loc
                    ++ ": cannot infer type of " ++ nodeTy ++ "\n"
        (Left msg, Left msg') -> Left $ (msg ++ msg') ++ show loc
                    ++ ": cannot infer type of " ++ nodeTy ++ "\n"

typeFunDec :: ([Located String], Located Exp) -> Either String Type
typeFunDec decs = typeCheck (map (\x -> (x, TPoly)) $ fst decs) $ snd decs

mergeTy :: Type -> Type -> Either String Type
mergeTy TPoly TPoly          = Right TPoly
mergeTy TNbr TNbr            = Right TNbr
mergeTy TStr TStr            = Right TStr
mergeTy TBool TBool          = Right TBool
mergeTy (TList t) (TList t') = either (Left) (Right . TList) $ mergeTy t t'
mergeTy t TPoly              = Right t
mergeTy TPoly t              = Right t
mergeTy a b                  = Left $
    "error " ++ show a ++ " can't be matched against " ++ show b ++ "\n"

mergeCmpTy :: Type -> Type -> Either String Type
mergeCmpTy TPoly TPoly = Right TBool
mergeCmpTy TPoly a     = Right TBool
mergeCmpTy a TPoly     = Right TBool
mergeCmpTy _ _         = Left "lol"

