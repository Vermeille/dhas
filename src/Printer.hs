#! /usr/bin/env runhugs +l
--
-- Printer.hs
-- Copyright (C) 2013 vermeille <guillaume.sanchez@epita.fr>
--
-- Distributed under terms of the MIT license.
--

module Printer where

import Ast
import Lexer

showInstr :: Int -> Located Instr -> String
showInstr ind (L _ (Instr exp)) = indent ind ++ showExp ind exp ++ ";\n"
showInstr ind (L _ (Affect var val)) = indent ind
        ++ showLvalue ind var ++ " = " ++ showExp ind val ++ ";\n"
showInstr ind (L _ (Dec ty var)) = indent ind ++ showType ind ty ++ " "
        ++ showLvalue ind var ++ ";\n"

showInstrs ind instrs = foldr (\s res -> showInstr ind s ++ res) "" instrs

showLvalue :: Int -> Located Lvalue -> String
showLvalue ind (L _ (Var str)) = str

showType :: Int -> Located TypeAst -> String
showType ind (L _ (TypeInt)) = "Int"

showExp :: Int -> Located Exp -> String
showExp ind (L _ (Add a b))   = showBin ind "+" a b
showExp ind (L _ (Sub a b))   = showBin ind "-" a b
showExp ind (L _ (Div a b))   = showBin ind "/" a b
showExp ind (L _ (Mul a b))   = showBin ind "*" a b
showExp ind (L _ (Less a b))  = showBin ind "<" a b
showExp ind (L _ (Great a b)) = showBin ind ">" a b
showExp ind (L _ (Leq a b))   = showBin ind "<=" a b
showExp ind (L _ (Geq a b))   = showBin ind ">=" a b
showExp ind (L _ (If a b c))  =
        indent ind ++ "if " ++ showExp ind a ++ "\n"
         ++ indent ind ++ " then\n"
             ++ indent (ind + 1) ++ showExp (ind + 1) b ++ "\n"
         ++ indent ind ++ "else\n"
             ++ indent (ind + 1) ++ showExp (ind + 1) c ++ "\n"
         ++ indent ind ++ "end\n"

showExp ind (L _ (ListLit exps)) =
        indent ind ++ "[\n"
        ++ foldr (showElt ind) "" exps
        ++ indent ind ++ "]\n"
        where
            showElt ind exp = (++) $ indent (ind + 1)
                                            ++ showExp (ind + 1) exp ++ ",\n"
showExp ind (L _ (Nbr n)) = show n
showExp ind (L _ (LVal val)) = showLvalue ind val

showBin ind op a b = "(" ++ showExp ind a ++ " "
                          ++ op ++ " " ++ showExp ind b ++ ")"

indent :: Int -> String
indent n = replicate (4 * n) ' '
