#! /usr/bin/env runhugs +l
--
-- Ast.hs
-- Copyright (C) 2013 vermeille <guillaume.sanchez@epita.fr>
--
-- Distributed under terms of the MIT license.
--

module Ast where

import Lexer

data Exp = Add      (Located Exp) (Located Exp)
         | Sub      (Located Exp) (Located Exp)
         | Div      (Located Exp) (Located Exp)
         | Mul      (Located Exp) (Located Exp)
         | Less     (Located Exp) (Located Exp)
         | Great    (Located Exp) (Located Exp)
         | Leq      (Located Exp) (Located Exp)
         | Geq      (Located Exp) (Located Exp)
         | If       (Located Exp) (Located Exp) (Located Exp)
         | ListLit  [Located Exp]
         | Nbr      Int
         | Paren    (Located Exp)
         | LVal   (Located Lvalue)

data Lvalue = Var String

data Instr = Instr  (Located Exp)
           | Affect (Located Lvalue) (Located Exp)
           | Dec    (Located TypeAst) (Located Lvalue)

data TypeAst = TypeInt
             | TypeStr

unloc (L _ node) = node
