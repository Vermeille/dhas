{

module Parser where

import Data.Char
import Prelude
import Ast
import Lexer
import Printer

}

%name parse
%tokentype { Located TokenType }
%error { parseError }
%monad { Parser } { thenP } { returnP }
%lexer { lexer } { L _ TokEOF }

%token
    "="                              { L loc TokEq }
    "+"                              { L loc TokPlus }
    "-"                              { L loc TokMinus }
    "*"                              { L loc TokMul }
    "/"                              { L loc TokDiv }
    "("                              { L loc TokOPar }
    ")"                              { L loc TokCPar }
    ","                              { L loc TokComma }
    "<"                              { L loc TokLess }
    ">"                              { L loc TokMoar }
    "<="                             { L loc TokLeq }
    ">="                             { L loc TokMeq }
    var                              { L loc (TokVar $$) }
    nbr                              { L loc (TokNbr $$) }
    "if"                             { L loc TokIf }
    "else"                           { L loc TokElse }
    "["                              { L loc TokOBrack }
    "]"                              { L loc TokCBrack }
    ";"                              { L loc TokSemiColon }
    "int"                            { L loc TokInt }

%left "="
%left "else"
%left "<" "<=" ">" ">="
%left "+" "-"
%left "*" "/"

%%

Instrs :: { [Located Instr] }
Instrs : Instr Instrs                   { $1:$2 }
       | Type Lvalue "=" Exp ";" Instrs { (L (getLoc $1) $ Dec $1 $2)
                                          :(L (getLoc $1) $ Affect $2 $4):$6 }
       | {- empty -}                    { [] }

Instr :: { Located Instr }
Instr : Exp ";"                      { L (getLoc $2) $ Instr $1 }
      | Affect                       { $1 }
      | Dec                          { $1 }

Dec :: { Located Instr }
Dec : Type Lvalue ";"        { L (getLoc $1) $ Dec $1 $2 }

Affect :: { Located Instr }
Affect : Lvalue "=" Exp ";"          { L (getLoc $1) $ Affect $1 $3 }

Exp :: { Located Exp }
Exp : LvalueExp                      { $1 }
    | nbr                            { L loc $ Nbr $1 }
    | Exp "+" Exp                    { L (getLoc $2) $ Add $1 $3 }
    | Exp "-" Exp                    { L (getLoc $2) $ Sub $1 $3 }
    | Exp "/" Exp                    { L (getLoc $2) $ Div $1 $3 }
    | Exp "*" Exp                    { L (getLoc $2) $ Mul $1 $3 }
    | Exp "<" Exp                    { L (getLoc $2) $ Less $1 $3 }
    | Exp ">" Exp                    { L (getLoc $2) $ Great $1 $3 }
    | Exp "<=" Exp                   { L (getLoc $2) $ Leq $1 $3 }
    | Exp ">=" Exp                   { L (getLoc $2) $ Geq $1 $3 }
    | "(" Exp ")"                    { L (getLoc $1) $ Paren $2 }
    | ListLit                        { $1 }

LvalueExp :: { Located Exp }
LvalueExp : Lvalue                   { L (getLoc $1) $ LVal $1 }

Lvalue :: { Located Lvalue }
Lvalue : var                         { L (loc) $ Var $1 }

ListLit : "[" ListParam "]"          { L (getLoc $1) $ ListLit $2 }

ListParam : Exp "," ListParam        { $1:$3 }
          | Exp                      { [$1] }

Type :: { Located TypeAst }
Type : "int"                         { L (getLoc $1) TypeInt }

{

parseError :: Located TokenType -> Parser a
parseError tok (s, l, c) = fail $ "parse error at `" ++ show (take 10 s)
    ++ "' line " ++ show l ++ " on token: " ++ show tok

}

